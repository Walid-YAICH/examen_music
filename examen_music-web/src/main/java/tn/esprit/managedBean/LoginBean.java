package tn.esprit.managedBean;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.entity.DJ;
import tn.esprit.service.DJService;

@ManagedBean
@SessionScoped
public class LoginBean {
	@EJB
	DJService djService;
	
	private String email;
	
	private String password;
	
	private DJ dj;
	
	private boolean loggedIn;
	
	public String doLogin(){
		String navigateTo="null";
		dj = djService.getDJByEmailAndPassword(email, password);
	    if (dj !=null){
	    	loggedIn=true;
			navigateTo="/welcome?faces-redirect=true";
		}
		else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("bad credentials"));
		}
	    System.out.println("dj.getPlaylists().size()" + dj.getPlaylists().size());
	    return navigateTo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public DJ getDj() {
		return dj;
	}

	public void setDj(DJ dj) {
		this.dj = dj;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

}
