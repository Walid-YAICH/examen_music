package tn.esprit.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.entity.DJ;
import tn.esprit.entity.Playlist;
import tn.esprit.entity.Song;


@Stateless
@LocalBean
public class DJService implements DjServiceRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public void ajouter_DJ(DJ dj) {
		em.persist(dj);
	}

	@Override
	public void ajouter_Playlist(Playlist playlist) {
		em.persist(playlist);
	}

	@Override
	public void affecter_Playlist_A_DJ(Long playlistId, Long djId) {
		Playlist playlist = em.find(Playlist.class, playlistId);
		DJ dj = em.find(DJ.class, djId);

		int nb_playlist = dj.getPlaylists().size();
		if(nb_playlist > 0){
			dj.getPlaylists().add(playlist);
		} else{
			List<Playlist> playlists = new ArrayList<>();
			playlists.add(playlist);
			dj.setPlaylists(playlists);
		}
	}

	@Override
	public void ajouter_Song_et_affecter_a_Playlist(Song song, Long playlistId) {
		em.persist(song);
		song.setPlaylist(new Playlist(playlistId));
	}

	@Override
	public List<Playlist> recuperer_Playlist_Par_DJ(Long djId) {
		return em.find(DJ.class, djId).getPlaylists();
	}
	
	@Override
	public Long nombre_total_songs() {
		TypedQuery<Long> query = em.createQuery("select count(s) from Song s", Long.class);
		return query.getSingleResult() ;
	}
	
	@Override
	public DJ getDJByEmailAndPassword(String email, String password) {
		TypedQuery<DJ> query= em.createQuery("Select d from DJ d where d.email=:email and d.password=:password", DJ.class);
		query.setParameter("email", email);
		query.setParameter("password", password);
		DJ dj = null;
		try{
			dj = query.getSingleResult();
		}
		catch(NoResultException e){
			System.out.println("Not found");
		}
		return dj;
	}

	
	
	//not used
	@Override
	public List<Song> recuperer_songs_par_DJ_et_duration() {
		return em.createQuery("select s from Song s join s.playlist p join p.......").getResultList();
	}

}
