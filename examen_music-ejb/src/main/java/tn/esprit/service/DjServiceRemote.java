package tn.esprit.service;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entity.DJ;
import tn.esprit.entity.Playlist;
import tn.esprit.entity.Song;

@Remote
public interface DjServiceRemote {
	void ajouter_DJ(DJ dj);
    void ajouter_Playlist(Playlist playlist);
    void affecter_Playlist_A_DJ(Long playlistId, Long djId);
    void ajouter_Song_et_affecter_a_Playlist(Song song, Long playlistId);
    List<Playlist> recuperer_Playlist_Par_DJ(Long djId);
    Long nombre_total_songs();
    List<Song> recuperer_songs_par_DJ_et_duration();
	DJ getDJByEmailAndPassword(String email, String password);
}
