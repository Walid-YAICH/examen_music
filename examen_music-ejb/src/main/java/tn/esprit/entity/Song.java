package tn.esprit.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Song implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	Long id;
	
	String name;
	
	Integer duration;
	
	Style style;
	
	@ManyToOne
	Playlist playlist;

	
	public Song() {
		super();
	}

	
	public Song(Long id, String name, Integer duration, Style style) {
		super();
		this.id = id;
		this.name = name;
		this.duration = duration;
		this.style = style;
	}




	public Song(String name, Integer duration, Style style) {
		super();
		this.name = name;
		this.duration = duration;
		this.style = style;
	}

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}
	
	
	
}
