package tn.esprit.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Playlist implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	Long id;
	
	String name;
	
	Integer duration;
	
	@OneToMany(mappedBy="playlist", fetch=FetchType.EAGER)
	private List<Song> songs;
	
	
	public Playlist(Long id, String name, Integer duration) {
		super();
		this.id = id;
		this.name = name;
		this.duration = duration;
	}

	public Playlist(String name, Integer duration) {
		super();
		this.name = name;
		this.duration = duration;
	}

	public Playlist() {
		super();
	}

	public Playlist(Long id) {
		super();
		this.id = id;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	
	
	
}

