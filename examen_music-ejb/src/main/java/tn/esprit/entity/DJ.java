package tn.esprit.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class DJ implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	Long id;
	
	String email;
	
	String password;
	
	String name;
	
	String address;

	@OneToMany(fetch=FetchType.EAGER)
	private List<Playlist> playlists;
	
	
	public DJ() {
		super();
	}
	
	
	
	public DJ(Long id, String email, String password, String name, String address) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.name = name;
		this.address = address;
	}



	public DJ(String email, String password, String name, String address) {
		super();
		this.email = email;
		this.password = password;
		this.name = name;
		this.address = address;
	}
	
	public List<Playlist> getPlaylists() {
		return playlists;
	}

	public void setPlaylists(List<Playlist> playlists) {
		this.playlists = playlists;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
}
